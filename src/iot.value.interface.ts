import { Document } from 'mongoose';

export interface IoTValue extends Document {
  edgeComponent: string;
  sensor: string;
  value: any;
  createdAt: Date;
}
