import * as mongoose from 'mongoose';

export const IoTValuesSchema: mongoose.Schema = new mongoose.Schema({
  value: String,
  edgeComponent: String,
  sensor: String,
  createdAt: Date,
});
