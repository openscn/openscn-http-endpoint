import { IsUUID, IsOptional } from 'class-validator';

export class PostValueDTO {
  @IsUUID(4)
  edgeComponent: string;

  @IsUUID(4)
  sensor: string;

  @IsOptional()
  value: string | number;
}
